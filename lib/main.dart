import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('My Business Card'),
            backgroundColor: Colors.cyan,
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.arrow_forward),
          ),
          body: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CircleAvatar(
                  radius: 50.0,
                  backgroundImage: AssetImage('images/fb_pic.jpg'),
                ),
                SizedBox(
                  height: 10,
                ),
                Text('Gopinath Sreenivasan',
                    style: TextStyle(
                        fontFamily: 'Pacifico',
                        fontSize: 25,
                        color: Colors.deepPurple)),
                Text('FLUTTER DEVELOPER',
                    style: TextStyle(
                        fontFamily: 'SourceSansPro',
                        fontSize: 16,
                        color: Colors.red)),
                SizedBox(
                  height: 10,
                  width: 150,
                  child: Divider(
                    thickness: 2,
                    color: Colors.cyan,
                  ),
                ),
                Card(
                    color: Colors.grey,
                    margin: EdgeInsets.only(left: 40, right: 40),
                    child: ListTile(
                      leading: Icon(Icons.phone_android),
                      title: Text(
                        '+46-12345678',
                        style: TextStyle(
                            color: Colors.yellow,
                            fontFamily: 'SourceSansPro',
                            letterSpacing: 2),
                      ),
                    )),
                SizedBox(
                  height: 2,
                ),
                Card(
                    color: Colors.amber,
                    margin: EdgeInsets.only(left: 40, right: 40),
                    child: ListTile(
                      leading: Icon(Icons.email),
                      title: Text(
                        'learningflutter@email.com',
                        style: TextStyle(
                            color: Colors.teal,
                            fontFamily: 'SourceSansPro',
                            letterSpacing: 0),
                      ),
                    ))
              ],
            ),
          )),
    );
  }
}
